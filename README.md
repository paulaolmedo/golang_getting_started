# Recursos golang 📖


## Generalidades

### Documentación oficial ⚖️

- [Effective go](https://go.dev/doc/effective_go)  

- [How to write go code](https://go.dev/doc/code)  

- [A tour of go](https://go.dev/tour/welcome/1)

### Documentación (casi) oficial 💭

- [The acme of foolishness](https://dave.cheney.net/practical-go)

    Destacado -> [Solid GO design](https://dave.cheney.net/2016/08/20/solid-go-design)
- [Go training: Ardan labs](https://github.com/ardanlabs/gotraining)

- [You don't know go yet](https://ydkgo.netlify.app/)
  
## Testing

### Para leer 🧠

- [Prefer table driven tests](https://dave.cheney.net/2019/05/07/prefer-table-driven-tests)

- [Mocking Techniques for Go](https://www.myhatchpad.com/insight/mocking-techniques-for-go/)

### Algunas librerías 🔎

- [Testing](https://pkg.go.dev/testing)

- [Testify](https://github.com/stretchr/testify)

- [Godog](https://github.com/cucumber/godog)

- [Ginkgo](https://github.com/onsi/ginkgo)

## Concurrencia 🏃

- [Learn concurrency](https://github.com/golang/go/wiki/LearnConcurrency  )

